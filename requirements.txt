# pip install --allow-external pywapi --allow-unverified pywapi -r requirements.txt
Flask==0.10.1
Flask-BasicAuth==0.2.0
Flask-MySQL==1.2
MySQL-python==1.2.5
flask-lesscss==0.9.1
numpy==1.9.1
plotly==1.4.7
pywapi==0.3.8